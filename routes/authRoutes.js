const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const { email, password } = req.body;
        const user = AuthService.login({ email });

        if (password === user.password) {
            res.status(200)
            res.data = user;
        } else {
            res.status(400).send({
                error: true,
                message: 'Wrong password.'
            });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;