const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    const allFighters = FighterService.getAllFighters()
    res.data = allFighters;

    next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    const fighter = req.fighterData;
    try {
        res.status(200).send(FighterService.createFighter(fighter));
    } catch (err) {
        res.status(400).send({
            error: true,
            message: 'Cannot create fighter!'
        });
    }
});

router.get('/:id', (req, res, next) => {
    const { id } = req.params;
    try {
        const fighter = FighterService.search({ id });
        if (!fighter) {
            return res.status(404).send({
                error: true,
                message: 'User not found!'
            });
        }
        res.send(fighter);
        next();
    } catch (err) {
        res.status(400).send({
            error: true,
            message: 'Internal server error!'
        });
    }
}, responseMiddleware);

router.delete('/:id', (req, res) => {
    const { id } = req.params;
    try {
        const deleteFighter = FighterService.deleteById(id);
        res.status(200).send(deleteFighter);
    } catch (err) {
        res.status(404).send({
            error: true,
            message: 'Fighter not found!'
        });
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res) => {
    const id = req.params.id;
    const fighterData = req.body;
    try {
        res.send(FighterService.updateFighter(id, fighterData));
    } catch (err) {
        res.status(404).send({
            error: true,
            message: 'User not found!'
        });
    }
})

module.exports = router;