const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { serialize } = require('lowdb/adapters/FileSync');

const router = Router();

router.post('/', createUserValid, (req, res) => {
    const user = req.userData;
    try {
        res.send(UserService.create(user));
    } catch (err) {
        res.status(400).send({
            error: true,
            message: 'Cannot create user!'
        });
    }
});

router.get('/', (req, res) => {
    try {
        const users = UserService.getUsers();
        if (users && users.length) {
            res.status(200);
            res.data = users;
        }
    } catch (err) {
        res.status(400).send({
            error: true,
            message: 'Internal server error!'
        });
    }
}, responseMiddleware);

router.get('/:id', (req, res) => {
    const { id } = req.params;
    try {
        const user = UserService.search({ id });

        if (!user) {
            return res.status(404).send({
                error: true,
                message: 'User not found!'
            });
        }
        res.send(user);
    } catch (err) {
        res.status(400).send({
            error: true,
            message: 'Internal server error!'
        });
    }
}, responseMiddleware);

router.delete('/:id', (req, res) => {
    const { id } = req.params;
    try {
        const deleteUser = UserService.deleteById(id);
        res.status(200).send(deleteUser);
    } catch (err) {
        res.status(404).send({
            error: true,
            message: 'User not found!'
        });
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res) => {
    const id = req.params.id;
    const userData = req.body;
    try {
        res.send(UserService.updateUser(id, userData));
    } catch (err) {
        res.status(404).send({
            error: true,
            message: 'User not found!'
        });
    }
})

module.exports = router;