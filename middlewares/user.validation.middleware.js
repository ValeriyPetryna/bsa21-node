const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const userData = req.body;
    const userModel = Object.assign({}, user);
    delete userModel.id;

    try {
        if (!Object.keys(userModel).every(key => userData.hasOwnProperty(key))) {
            return res.status(400).send({
                error: true,
                message: 'All fields must be filled in.'
            });
        }
        const errors = validationRules(UserService, req.body);

        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors.join('\n')
            });
        } else {
            req.userData = userData;
            next();
        }

    } catch (err) {
        res.status(404).send({
            error: true,
            message: err
        });
    }
}

const updateUserValid = (req, res, next) => {
    const userData = req.body;
    const userModel = Object.assign({}, user);
    delete userModel.id;

    try {
        if (!Object.keys(userModel).every(key => userData.hasOwnProperty(key))) {
            res.status(400).send({
                error: true,
                message: 'All fields must be filled in.'
            });
        } else {
            req.userData = userData;
            next();
        }
    } catch (err) {
        res.status(404).send({
            error: true,
            message: err
        });
    }
}

const minLength = (value, min) => value.trim().length >= min;
const maxLength = (value, max) => value.trim().length <= max;
const phoneNumberRule = (phone) => phone.match(/^\+380[0-9]{9}$/);
const uniqueEmail = (service, email) => !service.search({ email: email });
const emailRule = (email) => {
    const reg = /^[a-z0-9]((\.|\+)?[a-z0-9]){2,50}@gmail\.com$/i;

    return email.match(reg);
};

const validationRules = (service, user) => {
    const errors = [];

    if (!phoneNumberRule(user.phoneNumber)) {
        errors.push('Please enter a valid phone number (+380xxxxxxxxx).');
    }

    if (!minLength(user.firstName, 2) || !maxLength(user.firstName, 46)) {
        errors.push('Incorrect first name.');
    }

    if (!minLength(user.lastName, 2) || !maxLength(user.lastName, 46)) {
        errors.push('Incorrect last name.');
    }

    if (!minLength(user.password, 3) || !maxLength(user.password, 64)) {
        errors.push('Password should have 3 - 50 characters.');
    }

    if (!emailRule(user.email)) {
        errors.push('Please enter valid email @gmail.com.');
    }

    if (!uniqueEmail(service, user.email)) {
        errors.push('The email was already registered.');
    }

    return errors;
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;