const responseMiddleware = (req, res, next) => {
    if (req.err) {
        const code = req.err.code || 404;
        const message = req.err.message || 'Server error';
        res.status(code).json({
            error: true,
            message: message
        });
    }
    if (res.data) {
        res.status(200).send(res.data);
        next();
    }
}

exports.responseMiddleware = responseMiddleware;