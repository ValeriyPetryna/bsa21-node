const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    const fighterData = req.body;
    const fighterModel = Object.assign({}, fighter);

    fighterData.id = FighterRepository.generateId();
    fighterData.health = fighter.health;

    try {
        if (!Object.keys(fighterModel).every(key => fighterData.hasOwnProperty(key))) {
            res.status(400).send({
                error: true,
                message: 'All fields must be filled in.'
            });
        }
        const errors = validationRules(FighterService, fighterData);

        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors.join('\n')
            });
        } else {
            req.fighterData = fighterData;
            next();
        }
    } catch (err) {
        res.status(404).send({
            error: true,
            message: err
        });
    }
}

const updateFighterValid = (req, res, next) => {
    const fighterData = req.body;
    const fighterModel = Object.assign({}, fighter);

    try {
        if (!Object.keys(fighterModel).every(key => fighterData.hasOwnProperty(key))) {
            res.status(400).send({
                error: true,
                message: 'All fields must be filled in.'
            });
        }
        const errors = updateValidationRules(FighterService, req.body);

        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors.join('\n')
            });
        } else {
            req.fighterData = fighterData;
            next();
        }
    } catch (err) {
        res.status(404).send({
            error: true,
            message: err
        });
    }
}

const minValue = (value, min) => value >= min;
const maxValue = (value, max) => value <= max;
const uniqueName = (service, name) => !service.search({ name: name });
const uniqueId = (service, id) => !service.search({ id: id });

const validationRules = (service, fighter) => {
    const errors = [];

    if (!minValue(fighter.power, 1) || !maxValue(fighter.defense, 10)) {
        errors.push(`Power should have value from 1 to 10. Current power: ${fighter.power}`);
    }
    if (!minValue(fighter.defense, 1) || !maxValue(fighter.defense, 10)) {
        errors.push(`Defense should have value from 1 to 10. Current defense: ${fighter.defense}`);
    }

    if (!uniqueName(service, fighter.name) || !uniqueId(service, fighter.id)) {
        errors.push(`Fighter name and id should be unique.`);
    }

    return errors;
};

const updateValidationRules = (service, fighter) => {
    const errors = [];

    if (!minValue(fighter.power, 1) || !maxValue(fighter.defense, 10)) {
        errors.push(`Power should have value from 1 to 10. Current power: ${fighter.power}`);
    }
    if (!minValue(fighter.defense, 1) || !maxValue(fighter.defense, 10)) {
        errors.push(`Defense should have value from 1 to 10. Current defense: ${fighter.defense}`);
    }

    return errors;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;