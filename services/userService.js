const { UserRepository } = require('../repositories/userRepository');
class UserService {

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(userData) {
        const ifUserExist = UserRepository.getOne({
            email: userData.email
        });

        if (!ifUserExist) {
            const item = UserRepository.create(userData);
            if (!item) {
                throw Error('Server error!');
            }
            return item;
        } else {
            throw Error('Account with selected email is already exists!');
        }
    }

    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            throw Error('No users in db!');
        }
        return users;
    }

    deleteById(id) {
        const ifUserExist = UserRepository.getOne({
            id: id
        });
        if (!ifUserExist) {
            throw Error('User not found!');
        }
        const deleteUser = UserRepository.delete(id);

        return deleteUser;
    }

    updateUser(id, userData) {
        const ifUserExist = UserRepository.getOne({
            id: id
        });
        if (!ifUserExist) {
            throw Error('User not found!');
        } else return UserRepository.update(id, userData)
    }
}

module.exports = new UserService();