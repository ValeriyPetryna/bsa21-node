const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
    getAllFighters() {
        return FighterRepository.getAll();
    }

    createFighter(fighterData) {
        const ifFighterExist = FighterRepository.getOne({
            id: fighterData.id
        });

        if (!ifFighterExist) {
            const item = FighterRepository.create(fighterData);
            if (!item) {
                throw Error('Server error!');
            }
            return item;
        } else {
            throw Error('Figher with same id exist!');
        }
    }

    deleteById(id) {
        const ifFighterExist = FighterRepository.getOne({
            id: id
        });
        if (!ifFighterExist) {
            throw Error('Fighter not found!');
        }
        const deleteUser = FighterRepository.delete(id);

        return deleteUser;
    }

    updateFighter(id, fighterData) {
        const ifFighterExist = FighterRepository.getOne({
            id: id
        });
        if (!ifFighterExist) {
            throw Error('Fighter not found!');
        } else return FighterRepository.update(id, fighterData)
    }
}

module.exports = new FighterService();